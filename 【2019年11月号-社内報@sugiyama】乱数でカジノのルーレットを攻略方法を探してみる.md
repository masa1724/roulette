<style>
html, body {
  width: 100%;
  height: 100%;
}

body {
  font-family: "游ゴシック" !important;
  font-size: 14px !important;
}

h1 {
  font-size:2em !important;
}

h2 {
    border-left: 5px solid #1B95E0;
    padding-left: 8px;
    border-radius: 4px;
}

section {
  border: solid 1px #000;    	/* 内側の線になる一本線の枠線をひく*/
  outline: solid 2px #000;    /* 外側の線になる5pxの一本線の枠線をひく*/
  outline-offset: 4px;        /* 内側の線になる一本線の枠線をひく*/
  margin: 8px 6px;
  padding: 0 35px !important;
  min-height: 1038px;
  page-break-before: always;
}

pre[class*="language-"] {
    border: solid 1px #CCC !important;
    box-sizing: border-box;
}

. table {
  box-sizing: border-box;
  border: solid 1px #DDD;
}

.table tr {
  width: 100%;
}

.table td {
  width: 50%;
}

img {
 height: 400px;
 width: auto;
}
</style>
<style>
html, body {
  width: 100%;
  height: 100%;
}

body {
  font-family: "游ゴシック" !important;
  font-size: 14px !important;
}

h1 {
  font-size:2em !important;
}

h2 {
    border-left: 5px solid #1B95E0;
    padding-left: 8px;
    border-radius: 4px;
}

section {
  border: solid 1px #000;    	/* 内側の線になる一本線の枠線をひく*/
  outline: solid 2px #000;    /* 外側の線になる5pxの一本線の枠線をひく*/
  outline-offset: 4px;        /* 内側の線になる一本線の枠線をひく*/
  margin: 8px 6px;
  padding: 0 35px !important;
  min-height: 1038px;
  page-break-before: always;
}

pre[class*="language-"] {
    border: solid 1px #CCC !important;
    box-sizing: border-box;
}

. table {
  box-sizing: border-box;
  border: solid 1px #DDD;
}

.table tr {
  width: 100%;
}

.table td {
  width: 50%;
}

img {
 height: 400px;
 width: auto;
}
</style>

<section>

# 乱数でカジノのルーレットを攻略方法を探してみる

## はじめに

流行りからはだいぶ遅れましたが、  
最近タピオカミルクティーデビューをしました@sugiyamaです。

はじめて飲んでみた感想としては、味(抹茶ミルクティー味)自体は普通においしかったのですが、  
タピオカがストローに詰まってとても飲みづらかったです。(私の飲み方が悪いだけでしょうか・・・？なんとなくタピオカ1粒それぞれがくっついていたせいな感じがしました。)

あと、詰まりながらも頑張ってすすっていたら最後タピオカだけが余ってしまい、  
タピオカだけ食べるのは味が薄くてつらかったです。

今後、タピオカミルクティーを飲んでいく中で正しい飲み方を勉強していきたいと思います。

<br>
というかタピオカミルクティーって、数年に1回流行りが来ているような気がするのは私だけでしょうか。
[https://to-jin.net/tapioca/]によると2000年、2008年、2019年と約10年周期で流行しているとのことです。

前々から思っていたのですが、誰かの陰謀で一定周期で日本でタピオカを流行らせるよう裏で意図を引いているのではないでしょうか。  
今回実際に飲んでみて何度も流行るほどものすごくおいしいという印象もなかった(個人的な主観ですが)ため、  疑念が確証に変わりつつあります。

<br>
話は変わりますが、  
前回は乱数を駆使してアート？を描くという題目でやっていましたが、  
いまいち乱数を活かす良い題材が思いつかず個人的には消化不良となりました。。。

今回ですが、前回同様に乱数を駆使して何か取り組めるものはないかと考えていたところ、  
宝くじやカジノのルーレットなどのギャンブルの中で何か検証できることがあるのではないかという発想に至りました。  
具体的にはカジノのルーレットで賭け方をランダムに切り替えながら様々な組み合わせを試しその結果から逆算することで、勝率の高い賭け方が見つけられるのではないかという考えです。

## 今月号について

今月は検証用のルーレット作成に時間がかかったため、あまり試せていません。すいません。  
来月号の際には検証結果を載せたいと思います。

</section>
<section>

## 成果物
現在作成段階ですが、最低限は動きます。(一部バグってます。)

- ソースコード： https://gitlab.com/masa1724/roulette
- ＷＥＢ　　　：https://masa1724.gitlab.io/roulette/

 ![img](/assets/img.png)

## 最後に
前回もそうですが、タイトルに対して記事の内容が薄く申し訳ないです。  
基本引き込もりの私ですが、今月は珍しく現在新しいアパートを探したりと忙しく、現場作業も忙しかったためあまり時間がありませんでした。。。

To: suzukiさん  
前月の社内報でタピオカミルクティーのお店教えてほしいとありましたが、冒頭のタピオカミルクティーで、鶯谷駅北口から徒歩三分ほどの場所に「CAFE phrodite（アフロダイティー）」というタピオカミルクティーのお店があります。  
2019/02月にオープンされて割と新しめのお店で、中は狭い(3席)ですが、内装はきれいでした。(持ち帰りは可でした。)   
強くはおすすめしませんが、鶯谷に立ち寄る機会がありましたら足を運んでみるとどうでしょうか。

以上。(@sugiyama)

</section>
