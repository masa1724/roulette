import Color from "@/module/Color";

class Matcher {
  //-------------------------------------------------------------
  // インサイドベット
  //-------------------------------------------------------------

  /** 1目 */
  static straightUp(pocket, number) {
    return pocket.getNumber() === number;
  }

  // TODO  2～6目は追加対応予定
  // https://onlinecasino-tips.com/roulette.html

  //-------------------------------------------------------------
  // アウトサイドベット
  //-------------------------------------------------------------

  /** 縦一列 */
  static column1(pocket) {
    return pocket.getNumber() !== 0 && (pocket.getNumber() - 1) % 3 === 0;
  }
  static column2(pocket) {
    return pocket.getNumber() !== 0 && (pocket.getNumber() - 2) % 3 === 0;
  }
  static column3(pocket) {
    return pocket.getNumber() !== 0 && pocket.getNumber() % 3 === 0;
  }

  /** ダース */
  static dozen1(pocket) {
    return pocket.getNumber() >= 1 && pocket.getNumber() <= 12;
  }
  static dozen2(pocket) {
    return pocket.getNumber() >= 13 && pocket.getNumber() <= 24;
  }
  static dozen3(pocket) {
    return pocket.getNumber() >= 25 && pocket.getNumber() <= 36;
  }

  /** 前半 */
  static hi(pocket) {
    return pocket.getNumber() >= 1 && pocket.getNumber() <= 18;
  }
  /** 後半 */
  static low(pocket) {
    return pocket.getNumber() >= 19 && pocket.getNumber() <= 36;
  }

  /** 奇数 */
  static odd(pocket) {
    return pocket.getNumber() !== 0 && pocket.getNumber() % 2 === 1;
  }
  /** 偶数 */
  static even(pocket) {
    return pocket.getNumber() !== 0 && pocket.getNumber() % 2 === 0;
  }


  /** 赤 */
  static red(pocket) {
    return pocket.getNumber() !== 0 && pocket.getColor() === Color.Red;
  }
  /** 黒 */
  static black(pocket) {
    return pocket.getNumber() !== 0 && pocket.getColor() === Color.Black;
  }
  /** 緑 */
  static green(pocket) {
    return pocket.getNumber() === 0 && pocket.getColor() === Color.Green;
  }
}

export default Matcher;
