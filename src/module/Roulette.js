import RandamUtil from "./RandamUtil";
import Pockets from "./Pockets";

class Roulette {
  // ポケットを返す
  static throwIn() {
    const idx =RandamUtil.makeRangeRandom(0, Pockets.length - 1);
    return Pockets[idx];
  }
}

export default Roulette;
