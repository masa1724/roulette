class RandamUtil {
  // start~endまでの乱数を返す関数
  static makeRangeRandom(start, end) {
    return Math.floor(Math.random() * (end - start) + start);
  }
}

export default RandamUtil;
