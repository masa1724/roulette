class BettingResult {
  constructor(pocket, isHit, balance) {
    this.pocket = pocket;
    this.p_isHit = isHit;
    this.balance = balance;
  }

  getPocket() {
    return this.pocket;
  }

  isHit() {
    return this.p_isHit;
  }

  getBalance() {
    return this.balance;
  }
}

export default BettingResult;
