const SpeedMode = {
  Normal: 0,
  High: 1,
  Light: 2
};

export default SpeedMode;
