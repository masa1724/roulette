class Pocket {
  constructor(color, number) {
      this.color = color;
      this.number = number;
  }

  getColor() {
    return this.color;
  }

  getNumber() {
    return this.number;
  }
}

export default Pocket;
