const Color = {
  Green: Symbol(0),
  Red: Symbol(1),
  Black: Symbol(2)
};

export default Color;
