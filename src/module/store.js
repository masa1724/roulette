import Pocket from "@/module/Pocket";
import Color from "@/module/Color";
import SpeedMode from "@/module/SpeedMode";

const store = {
  state: {
    score: {
      color_red: 0,
      color_black: 0
    },
    roulette: {
      deg: 0
    },
    userSetting: {
      balance: 10000,
      wager: 500,
      tryCount: 2,
      readonly: false,
      selectedBettingType: "column1",
      speedMode: SpeedMode.Normal
    },
    result: {
      hitCount: 0,
      missCount: 0,
      hitPocket: new Pocket(Color.Green, "XXX"), // ダミー値をセット
      bettingResults: []
    }
  },
  clearScore() {
    const self = this;

    self.state.userSetting.balance = 10000;

    Object.keys(this.state.score).forEach(function (key) {
      self.state.score[key] = 0;
    });
    Object.keys(this.state.result).forEach(function (key) {
      if (key === "hitPocket") {
        self.state.result.hitPocket = new Pocket(Color.Green, "XXX");
      } else  if (key === "bettingResults") {
          self.state.result.bettingResults.splice(0); // 空配列だとwatchで検知出来ないためspliceで配列クリア
      } else {
        self.state.result[key] = 0;
      }
    });
  }
};

export default store;
