import Color from "@/module/Color";

class RoulletAppHelper {
  static copyToClipboard(text) {
    var tmpCopyArea = document.createElement("textarea");
    tmpCopyArea.textContent = text;

    var bodyElm = document.getElementsByTagName("body")[0];
    bodyElm.appendChild(tmpCopyArea);

    tmpCopyArea.select();
    document.execCommand('copy');

    bodyElm.removeChild(tmpCopyArea);
  }

  static getDisplayColorText(color) {
    if (color === Color.Red) {
      return "赤";
    } else if (color === Color.Black) {
      return "黒";
    } else {
      return "緑";
    }
  }

  static getDisplayHitText(isHit) {
    if (isHit) {
      return "当たり";
    } else {
      return "はずれ";
    }
  }
}

export default RoulletAppHelper;
