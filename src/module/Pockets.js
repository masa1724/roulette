import Color from "./Color";
import Pocket from "./Pocket";

const Pockets = [
  new Pocket(Color.Green, 0),
  new Pocket(Color.Red, 1),
  new Pocket(Color.Black, 2),
  new Pocket(Color.Red, 3),
  new Pocket(Color.Black, 4),
  new Pocket(Color.Red, 5),
  new Pocket(Color.Black, 6),
  new Pocket(Color.Red, 7),
  new Pocket(Color.Black, 8),
  new Pocket(Color.Red, 9),
  new Pocket(Color.Black, 10),
  new Pocket(Color.Red, 11),
  new Pocket(Color.Black, 12),
  new Pocket(Color.Red, 13),
  new Pocket(Color.Black, 14),
  new Pocket(Color.Red, 15),
  new Pocket(Color.Black, 16),
  new Pocket(Color.Red, 17),
  new Pocket(Color.Black, 18),
  new Pocket(Color.Red, 19),
  new Pocket(Color.Black, 20),
  new Pocket(Color.Red, 21),
  new Pocket(Color.Black, 22),
  new Pocket(Color.Red, 23),
  new Pocket(Color.Black, 24),
  new Pocket(Color.Red, 25),
  new Pocket(Color.Black, 26),
  new Pocket(Color.Red, 27),
  new Pocket(Color.Black, 28),
  new Pocket(Color.Red, 29),
  new Pocket(Color.Black, 30),
  new Pocket(Color.Red, 31),
  new Pocket(Color.Black, 32),
  new Pocket(Color.Red, 33),
  new Pocket(Color.Black, 34),
  new Pocket(Color.Red, 35),
  new Pocket(Color.Black, 36)
];

export default Pockets;
