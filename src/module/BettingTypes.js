import Matcher from "@/module/Matcher";

class BettingType {
  constructor(tactics, rate, displayName) {
      this.tactics = tactics;
      this.rate = rate;
      this.displayName = displayName;
  }
  isMatch(pocket) {return this.tactics(pocket);}
  getRate() {return this.rate;}
  getDisplayName() {return this.displayName;}
}

// キー名はstore.jsの「store.state.score」と合わせる
const BettingTypes = {
  column1: new BettingType((pocket) => Matcher.column1(pocket), 3, "縦一列 (1→34)"),
  column2: new BettingType((pocket) => Matcher.column2(pocket), 3, "縦一列 (2→35)"),
  column3: new BettingType((pocket) => Matcher.column3(pocket), 3, "縦一列 (3→36)"),
  dozen1: new BettingType((pocket) => Matcher.dozen1(pocket), 3, "大 (1～12)"),
  dozen2: new BettingType((pocket) => Matcher.dozen2(pocket), 3, "中 (13～24)	"),
  dozen3: new BettingType((pocket) => Matcher.dozen3(pocket), 3, "小 (25～36)"),
  hi: new BettingType((pocket) => Matcher.hi(pocket), 2, "前半 (1～18)"),
  low: new BettingType((pocket) => Matcher.low(pocket), 2, "後半 (19～36)"),
  odd: new BettingType((pocket) => Matcher.odd(pocket), 2, "奇数"),
  even: new BettingType((pocket) => Matcher.even(pocket), 2, "偶数"),
  red: new BettingType((pocket) => Matcher.red(pocket), 2, "赤"),
  black: new BettingType((pocket) => Matcher.black(pocket), 2, "黒")

};

export default BettingTypes;
